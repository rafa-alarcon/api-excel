package com.example.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.example.model.Customer;

public class ExcelGenerator {
	  
	  public static ByteArrayInputStream customersToExcel(List<Customer> customers) throws IOException {
	    String[] COLUMNs = {"Id", "Name", "Address", "Age"};
	    try(
	        Workbook workbook = new XSSFWorkbook();
	        ByteArrayOutputStream out = new ByteArrayOutputStream();
	    ){
	      CreationHelper createHelper = workbook.getCreationHelper();
	   
	      Sheet sheet = workbook.createSheet("Customers");
	   
	      Font headerFont = workbook.createFont();
	      headerFont.setBold(true);
	      headerFont.setColor(IndexedColors.BLUE.getIndex());
	   
	      CellStyle headerCellStyle = workbook.createCellStyle();
	      headerCellStyle.setFont(headerFont);
	   
	      // Row for Header
	      Row headerRow = sheet.createRow(0);
	   
	      // Header
	      for (int col = 0; col < COLUMNs.length; col++) {
	        Cell cell = headerRow.createCell(col);
	        cell.setCellValue(COLUMNs[col]);
	        cell.setCellStyle(headerCellStyle);
	      }
	   
	      // CellStyle for Age
	      CellStyle ageCellStyle = workbook.createCellStyle();
	      ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));
	   
	      int rowIdx = 1;
	      for (Customer customer : customers) {
	        Row row = sheet.createRow(rowIdx++);
	   
	        row.createCell(0).setCellValue(customer.getId());
	        row.createCell(1).setCellValue(customer.getName());
	        row.createCell(2).setCellValue(customer.getAddress());
	   
	        Cell ageCell = row.createCell(3);
	        ageCell.setCellValue(customer.getAge());
	        ageCell.setCellStyle(ageCellStyle);
	      }
	   
	      workbook.write(out);
	      return new ByteArrayInputStream(out.toByteArray());
	    }
	  }
	  public static ByteArrayInputStream listToExcel(List<Map<String, Object>> list) throws IOException {
		    String[] COLUMNs = {"Id", "Name", "Address", "Age"};
		    String[] cols = new String[list.get(0).size()];
		    int i = 0;
		    for (String key : list.get(0).keySet()) {
		    	cols[i] = key;
		    	i++;
		    }
		    try(
		        Workbook workbook = new XSSFWorkbook();
		        ByteArrayOutputStream out = new ByteArrayOutputStream();
		    ){
		      CreationHelper createHelper = workbook.getCreationHelper();
		   
		      Sheet sheet = workbook.createSheet("Customers");
		   
		      Font headerFont = workbook.createFont();
		      headerFont.setBold(true);
		      headerFont.setColor(IndexedColors.BLUE.getIndex());
		   
		      CellStyle headerCellStyle = workbook.createCellStyle();
		      headerCellStyle.setFont(headerFont);
		   
		      // Row for Header
		      Row headerRow = sheet.createRow(0);
		   
		      // Header
		      for (int col = 0; col < cols.length; col++) {
		        Cell cell = headerRow.createCell(col);
		        cell.setCellValue(cols[col]);
		        cell.setCellStyle(headerCellStyle);
		      }
		   
		      // CellStyle for Age
		      CellStyle ageCellStyle = workbook.createCellStyle();
		      ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));
		   
		      int rowIdx = 1;
		      for (Map<String, Object> ro : list) {
		        Row row = sheet.createRow(rowIdx++);
		        int rcntr = 0;
		        for (Map.Entry<String, Object> pair : ro.entrySet()) {
		        	if(pair.getValue() instanceof Integer) {
		        		row.createCell(rcntr).setCellValue((int) pair.getValue());
	        		} else if(pair.getValue() instanceof String) {
	        			row.createCell(rcntr).setCellValue((String)pair.getValue());
	        		} else if(pair.getValue() instanceof Double) {
		        		row.createCell(rcntr).setCellValue((double) pair.getValue());
	        		} else {
	        			row.createCell(rcntr).setCellValue("" + pair.getValue());
	        		}
		        	rcntr++;
		    	}
		      }
		   
		      workbook.write(out);
		      return new ByteArrayInputStream(out.toByteArray());
		    }
		  }
	}