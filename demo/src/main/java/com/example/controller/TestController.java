package com.example.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Map;
import java.util.Arrays;
 

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.InputStreamResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.util.ExcelGenerator;
import com.example.model.Customer;
@Controller
@RequestMapping("/")
public class TestController {
	
	 @Autowired
	 @Qualifier("jdbcTemplate")
	 private JdbcTemplate jdbcTemplate;
	 
	@RequestMapping(value = { "/", "" }, method = RequestMethod.GET)
	 public ModelAndView home() {
	  ModelAndView model = new ModelAndView();

	  model.setViewName("home");
	  return model;
	 } 
	
	@RequestMapping(value = "/export", method = RequestMethod.GET)
	 public ResponseEntity<InputStreamResource> export(ModelAndView model, HttpServletResponse response) throws IOException, SQLException {

	  //response.setContentType("application/x-download");
	  //response.setHeader("Content-Disposition", String.format("attachment; filename=\"test,pdf\""));
	  
	  HttpHeaders headers = new HttpHeaders();
	  headers.add("Content-Disposition", "attachment; filename=customers.xlsx");

	  //OutputStream out = response.getOutputStream();
	  List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT * FROM `menu`");
	  for (Map row : rows) {
			System.out.println(row);
		}
	  
	  List<Customer> customers = Arrays.asList(
	          new Customer(Long.valueOf(1), "Jack Smith", "Massachusetts", 23),
	          new Customer(Long.valueOf(2), "Adam Johnson", "New York", 27),
	          new Customer(Long.valueOf(3), "Katherin Carter", "Washington DC", 26),
	          new Customer(Long.valueOf(4), "Jack London", "Nevada", 33), 
	          new Customer(Long.valueOf(5), "Jason Bourne", "California", 36));
	    
	    //ByteArrayInputStream in = ExcelGenerator.customersToExcel(customers);
	    ByteArrayInputStream in = ExcelGenerator.listToExcel(rows);
	     //return IOUtils.toByteArray(in);
	    return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
	     
	 }
}
